module.exports = {
  userNotFound: 'User not found.',
  userAlreadyExists: 'User already exists.',
  userNotExists: 'User not exists.',
  incorrectPassword: 'Incorrect password.',
  internalError: 'Internal error.',
  youCannotAccess: 'You cannot modify or delete this resource.',
  notFound: 'Not Found.',
  commerceNotFound: 'Commerce not found.',
  commerceAlreadyExists: 'Commerce already exists.',
  commerceNotExists: 'Commerce not exists.',
  notAccessCreateRequest: 'You do not have access to create a payment request.',
  notFoundPayReq: 'Not found payment request.',
  payReqBloq: 'The payment request is blocked.',
  payReqPaid: 'The payment request is paid.',
  anyCards: 'Neither card meets the requirements',
  payCompleted: 'The payment was made correctly.'
};
