const errorMessages = require('./errorMessages');
const httpCodes = require('./httpCodes');
const pagination = require('./pagination');

module.exports = { errorMessages, httpCodes, pagination };
