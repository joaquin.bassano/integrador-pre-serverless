const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const CommerceSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    },
    cuil: {
      type: Number,
      required: true
    }
  },
  {
    timestamps: true,
    versionKey: false
  }
);

CommerceSchema.plugin(mongoosePaginate);

module.exports = {
  CommerceModel: mongoose.model('commerces', CommerceSchema),
  CommerceSchema
};
