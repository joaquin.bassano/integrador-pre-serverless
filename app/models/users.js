const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const UserSchema = new mongoose.Schema(
  {
    name: {
      type: String
    },
    age: {
      type: String
    },
    email: {
      type: String,
      required: true
    },
    password: {
      type: String
    },
    creditCard: {
      bank: { type: String, required: true },
      limit: { type: Number, required: true },
      active: { type: Boolean, default: true },
      debt: { type: Number, default: 0 },
      type: Object
    }
  },
  {
    timestamps: true,
    versionKey: false
  }
);

UserSchema.plugin(mongoosePaginate);

module.exports = { UserModel: mongoose.model('users', UserSchema), UserSchema };
