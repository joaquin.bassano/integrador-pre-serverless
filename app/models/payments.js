const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const { UserSchema } = require('./users');
const { PaymentRequestsSchema } = require('./payment-requests');

const PaymentsSchema = new mongoose.Schema(
  {
    paymentRequest: PaymentRequestsSchema,
    userPayer: UserSchema,
    callbackURLUser: {
      type: String
    }
  },
  {
    timestamps: true,
    versionKey: false
  }
);

PaymentsSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('payments', PaymentsSchema);
