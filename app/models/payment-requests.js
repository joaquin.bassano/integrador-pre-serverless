const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const { UserSchema } = require('./users');
const { CommerceSchema } = require('./commerce');

const PaymentRequestsSchema = new mongoose.Schema(
  {
    product: {
      type: String,
      required: true
    },
    amount: {
      type: Number,
      required: true
    },
    emailCommerce: {
      type: String
    },
    emailUser: {
      type: String
    },
    user: UserSchema,
    commerce: CommerceSchema,
    callbackURLCommerce: {
      type: String,
      default: process.env.WEBHOOK_COMMERCE
    },
    state: new mongoose.Schema(
      {
        active: {
          type: Boolean,
          default: true
        },
        attempts: {
          type: Number,
          default: 3
        },
        paid: {
          type: Boolean,
          default: false
        }
      },
      { versionKey: false, _id: false }
    )
  },
  {
    timestamps: true,
    versionKey: false
  }
);

PaymentRequestsSchema.plugin(mongoosePaginate);

module.exports = {
  PaymentsRequestsModel: mongoose.model(
    'paymentsRequests',
    PaymentRequestsSchema
  ),
  PaymentRequestsSchema
};
