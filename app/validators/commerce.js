const { check } = require('express-validator');
const { validateResult } = require('../helpers/validateHelper');

const validateCreate = [
  check('name').exists().not().isEmpty().isString(),

  check('email').exists().isEmail(),

  check('password').exists().not().isEmpty().isString(),

  check('cuil').exists().not().isEmpty().isNumeric(),

  (req, res, next) => {
    validateResult(req, res, next);
  }
];

const validateUpdate = [
  check('name').optional().isString(),

  check('email').optional().isEmail(),

  (req, res, next) => {
    validateResult(req, res, next);
  }
];

const validateLogin = [
  check('email').exists().isEmail(),

  check('password').exists().not().isEmpty().isString()
];

module.exports = { validateCreate, validateUpdate, validateLogin };
