const { check } = require('express-validator');
const { validateResult } = require('../helpers/validateHelper');

const validateCreate = [
  check('product').exists().not().isEmpty().isString(),

  check('amount')
    .exists()
    .isNumeric()
    .custom(value => {
      if (value < 0) {
        throw new Error('The amount must be greater than 0');
      }
      return true;
    }),

  check('state.active').optional().isBoolean(),

  check('state.attempts').optional().isNumeric(),

  check('state.paid').optional().isBoolean(),

  check('state').exists(),

  (req, res, next) => {
    validateResult(req, res, next);
  }
];

module.exports = { validateCreate };
