const { check } = require('express-validator');
const { validateResult } = require('../helpers/validateHelper');

const validateCreate = [
  check('name').exists().not().isEmpty().isString(),

  check('age')
    .exists()
    .isNumeric()
    .custom(value => {
      if (value < 18 || value > 40) {
        throw new Error('Age range must be between 18 and 40');
      }
      return true;
    }),

  check('email').exists().isEmail(),

  check('creditCard.*.bank').exists().not().isEmpty().isString(),

  check('creditCard.*.limit').exists().not().isEmpty().isNumeric(),

  check('creditCard.*.active').optional().isBoolean(),

  check('creditCard.*.debt')
    .exists()
    .not()
    .isEmpty()
    .isNumeric()
    .custom(value => {
      if (value < 0) {
        throw new Error('Debt must be greater than or equal to 0.');
      }
      return true;
    }),

  (req, res, next) => {
    validateResult(req, res, next);
  }
];

const validateUpdate = [
  check('name').optional().isString(),

  check('age')
    .optional()
    .isNumeric()
    .custom(value => {
      if (value < 18 || value > 40) {
        throw new Error('Age range must be between 18 and 40');
      }
      return true;
    }),

  check('email').optional().isEmail(),

  (req, res, next) => {
    validateResult(req, res, next);
  }
];

module.exports = { validateCreate, validateUpdate };
