const express = require('express');
const verifyToken = require('../middleware/verifyToken');

const router = express.Router();
const { generatePayment } = require('../controllers/payments');

router.post('/:id', verifyToken, generatePayment);

module.exports = router;
