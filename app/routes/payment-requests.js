const express = require('express');

const router = express.Router();
const {
  getPaymentRequests,
  payRequest
} = require('../controllers/payment-requests');
const verifyToken = require('../middleware/verifyToken');
const { validateCreate } = require('../validators/payment-requests');

router.get('/', verifyToken, getPaymentRequests);

router.post('/', validateCreate, verifyToken, payRequest);

module.exports = router;
