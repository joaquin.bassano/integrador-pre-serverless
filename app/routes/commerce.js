const express = require('express');

const router = express.Router();
const {
  getCommerces,
  getCommerce,
  createCommerce,
  updateCommerce,
  deleteCommerce,
  loginCommerce
} = require('../controllers/commerce');
const {
  validateCreate,
  validateUpdate,
  validateLogin
} = require('../validators/commerce');
const verifyToken = require('../middleware/verifyToken');
const verifyUser = require('../middleware/verifyUser');

router.get('/', verifyToken, getCommerces);

router.get('/:id', verifyToken, getCommerce);

router.post('/', validateCreate, createCommerce);

router.patch('/:id', validateUpdate, [verifyToken, verifyUser], updateCommerce);

router.delete('/:id', [verifyToken, verifyUser], deleteCommerce);

router.post('/login', validateLogin, loginCommerce);

module.exports = router;
