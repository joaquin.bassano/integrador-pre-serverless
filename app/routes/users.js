const express = require('express');

const router = express.Router();
const {
  getUsers,
  getUser,
  createUser,
  updateUser,
  deleteUser,
  loginUser,
  webhookUser
} = require('../controllers/users');
const { validateCreate, validateUpdate } = require('../validators/users');
const verifyToken = require('../middleware/verifyToken');
const verifyUser = require('../middleware/verifyUser');

router.get('/', verifyToken, getUsers);

router.get('/:id', verifyToken, getUser);

router.post('/', validateCreate, createUser);

router.patch('/:id', validateUpdate, [verifyToken, verifyUser], updateUser);

router.delete('/:id', [verifyToken, verifyUser], deleteUser);

router.post('/login', loginUser);

router.post('/webhook/:id', webhookUser);

module.exports = router;
