const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const asyncWrapper = require('../helpers/handleError');
const { UserModel } = require('../models/users');
const parseId = require('../utils/parseID');
const { errorMessages, httpCodes, pagination } = require('../constants');
const userService = require('../services/users');

const getUsers = asyncWrapper(async (req, res) => {
  const limit = parseInt(req.query.limit, 10) || pagination.limit;
  const page = parseInt(req.query.page, 10) || pagination.page;

  const listAll = await UserModel.paginate({}, { limit, page });
  return res.status(httpCodes.OK).json({ data: listAll });
});

const getUser = asyncWrapper(async (req, res) => {
  const { id } = req.params;
  const parsedId = parseId(id);

  const existsUser = await UserModel.findOne({ _id: parsedId });
  if (!existsUser)
    return res
      .status(httpCodes.NOT_FOUND)
      .json({ error: errorMessages.userNotFound });

  return res.status(httpCodes.OK).json(existsUser);
});

const createUser = asyncWrapper(async (req, res) => {
  const response = await userService.createUser(req.body);

  if (!response.isValid)
    return res.status(response.code).json({ error: response.error });

  return res.status(httpCodes.CREATED).json(response.data);
});

const updateUser = asyncWrapper(async (req, res) => {
  const { id } = req.params;
  const { body } = req;
  const parsedId = parseId(id);

  const existsUser = await UserModel.findOne({ _id: parsedId });
  if (!existsUser)
    return res
      .status(httpCodes.NOT_FOUND)
      .json({ error: errorMessages.userNotFound });

  const userUpdate = await UserModel.updateOne({ _id: parsedId }, body);
  return res.status(httpCodes.OK).json({ userUpdate });
});

const deleteUser = asyncWrapper(async (req, res) => {
  const { id } = req.params;
  const parsedId = parseId(id);

  const existsUser = await UserModel.findOne({ _id: parsedId });
  if (!existsUser)
    return res
      .status(httpCodes.NOT_FOUND)
      .json({ error: errorMessages.userNotFound });

  const userDelete = await UserModel.deleteOne({ _id: parsedId });
  return res.status(httpCodes.OK).json(userDelete);
});

const loginUser = asyncWrapper(async (req, res) => {
  const { email, password } = req.body;
  const existsUser = await UserModel.findOne({ email });
  if (!existsUser)
    return res
      .status(httpCodes.BAD_REQUEST)
      .json({ error: errorMessages.userNotExists });

  const isPasswordCorrect = await bcrypt.compare(password, existsUser.password);
  if (!isPasswordCorrect)
    return res
      .status(httpCodes.UNAUTHORIZED)
      .json({ error: errorMessages.incorrectPassword });

  const token = jwt.sign(
    { id: existsUser._id, email: existsUser.email },
    process.env.JWT_KEY,
    { expiresIn: process.env.JWT_EXPIRATION }
  );

  return res.status(httpCodes.OK).json({ token });
});

const webhookUser = asyncWrapper(async (req, res) => {
  const { emailUser, amount } = req.body.data.paymentRequest;
  const userUpdate = await UserModel.findOne({ email: emailUser });

  const index = userUpdate.creditCard.findIndex(
    item => item.bank === req.body.data.paymentRequest.user.creditCard.bank
  );

  userUpdate.creditCard[index].limit -= amount;

  userUpdate.creditCard = [
    ...userUpdate.creditCard,
    userUpdate.creditCard[index]
  ];
  userUpdate.creditCard = [...new Set(userUpdate.creditCard)];
  await userUpdate.save();

  return res.status(httpCodes.OK);
});

module.exports = {
  getUsers,
  getUser,
  createUser,
  updateUser,
  deleteUser,
  loginUser,
  webhookUser
};
