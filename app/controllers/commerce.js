const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const asyncWrapper = require('../helpers/handleError');
const { CommerceModel } = require('../models/commerce');
const parseId = require('../utils/parseID');
const { errorMessages, httpCodes, pagination } = require('../constants');

const getCommerces = asyncWrapper(async (req, res) => {
  const limit = parseInt(req.query.limit, 10) || pagination.limit;
  const page = parseInt(req.query.page, 10) || pagination.page;

  const listAll = await CommerceModel.paginate({}, { limit, page });
  return res.status(httpCodes.OK).json({ data: listAll });
});

const getCommerce = asyncWrapper(async (req, res) => {
  const { id } = req.params;
  const parsedId = parseId(id);

  const existsCommerce = await CommerceModel.findOne({ _id: parsedId });
  if (!existsCommerce)
    return res
      .status(httpCodes.NOT_FOUND)
      .json({ error: errorMessages.commerceNotFound });

  return res.status(httpCodes.OK).json(existsCommerce);
});

const createCommerce = asyncWrapper(async (req, res) => {
  const existsCommerce = await CommerceModel.findOne({ email: req.body.email });
  if (existsCommerce)
    return res
      .status(httpCodes.BAD_REQUEST)
      .json({ error: errorMessages.commerceAlreadyExists });

  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(req.body.password, salt);
  req.body.password = hash;

  const resDetail = await CommerceModel.create(req.body);

  const token = jwt.sign(
    { id: resDetail._id, email: resDetail.email },
    process.env.JWT_KEY,
    { expiresIn: process.env.JWT_EXPIRATION }
  );

  return res.status(httpCodes.CREATED).json({ data: resDetail, token });
});

const updateCommerce = asyncWrapper(async (req, res) => {
  const { id } = req.params;
  const { body } = req;
  const parsedId = parseId(id);

  const existsCommerce = await CommerceModel.findOne({ _id: parsedId });
  if (!existsCommerce)
    return res
      .status(httpCodes.NOT_FOUND)
      .json({ error: errorMessages.commerceNotFound });

  const commerceUpdate = await CommerceModel.updateOne({ _id: parsedId }, body);
  return res.status(httpCodes.OK).json({ commerceUpdate });
});

const deleteCommerce = asyncWrapper(async (req, res) => {
  const { id } = req.params;
  const parsedId = parseId(id);

  const existsCommerce = await CommerceModel.findOne({ _id: parsedId });
  if (!existsCommerce)
    return res
      .status(httpCodes.NOT_FOUND)
      .json({ error: errorMessages.commerceNotFound });

  const commerceDelete = await CommerceModel.deleteOne({ _id: parsedId });
  return res.status(httpCodes.OK).json(commerceDelete);
});

const loginCommerce = asyncWrapper(async (req, res) => {
  const { email, password } = req.body;
  const existsCommerce = await CommerceModel.findOne({ email });
  if (!existsCommerce)
    return res
      .status(httpCodes.BAD_REQUEST)
      .json({ error: errorMessages.commerceNotExists });

  const isPasswordCorrect = await bcrypt.compare(
    password,
    existsCommerce.password
  );
  if (!isPasswordCorrect)
    return res
      .status(httpCodes.UNAUTHORIZED)
      .json({ error: errorMessages.incorrectPassword });

  const token = jwt.sign(
    { id: existsCommerce._id, email: existsCommerce.email },
    process.env.JWT_KEY,
    { expiresIn: process.env.JWT_EXPIRATION }
  );

  return res.status(httpCodes.OK).json({ token });
});

module.exports = {
  getCommerces,
  getCommerce,
  createCommerce,
  updateCommerce,
  deleteCommerce,
  loginCommerce
};
