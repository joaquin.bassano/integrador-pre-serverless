const axios = require('axios');
const asyncWrapper = require('../helpers/handleError');
const { PaymentsRequestsModel } = require('../models/payment-requests');
const PaymentsModel = require('../models/payments');
const { UserModel } = require('../models/users');
const { httpCodes, errorMessages } = require('../constants');

const generatePayment = asyncWrapper(async (req, res) => {
  const { id } = req.params;

  const existsPaymentRequest = await PaymentsRequestsModel.findOne({ _id: id });
  if (!existsPaymentRequest)
    return res
      .status(httpCodes.NOT_FOUND)
      .json({ error: errorMessages.notFoundPayReq });

  const { paid, active } = existsPaymentRequest.state;

  if (paid || !active)
    return res.status(httpCodes.BAD_REQUEST).json({
      error: `${paid ? errorMessages.payReqPaid : errorMessages.payReqBloq}`
    });

  const existsUser = await UserModel.findOne({ email: req.user.email });
  if (!existsUser)
    return res
      .status(httpCodes.BAD_REQUEST)
      .json({ error: errorMessages.userNotExists });

  const creditCardSelected = existsUser.creditCard.find(
    item =>
      item.active &&
      item.debt === 0 &&
      item.limit >= existsPaymentRequest.amount
  );

  if (!creditCardSelected) {
    existsPaymentRequest.state.attempts--;
    if (existsPaymentRequest.state.attempts === 0)
      existsPaymentRequest.state.active = false;
    await existsPaymentRequest.save();

    return res
      .status(httpCodes.UNAUTHORIZED)
      .json({ error: errorMessages.anyCards });
  }

  existsUser.creditCard = creditCardSelected;
  existsPaymentRequest.emailUser = req.user.email;
  existsPaymentRequest.user = existsUser;
  existsPaymentRequest.state.paid = true;
  await existsPaymentRequest.save();

  const callbackURLUser = process.env.WEBHOOK_USER + existsUser._id;

  const resDetail = await PaymentsModel.create({
    paymentRequest: existsPaymentRequest,
    userPayer: existsUser,
    callbackURLUser
  });

  const axiosPost = url => {
    return axios.post(url, {
      info: errorMessages.payCompleted,
      data: resDetail
    });
  };

  Promise.allSettled([
    axiosPost(existsPaymentRequest.callbackURLCommerce),
    axiosPost(callbackURLUser)
  ]);

  return res.status(httpCodes.OK).json(resDetail);
});

module.exports = {
  generatePayment
};
