const asyncWrapper = require('../helpers/handleError');
const { PaymentsRequestsModel } = require('../models/payment-requests');
const { CommerceModel } = require('../models/commerce');
const { httpCodes, pagination, errorMessages } = require('../constants');

const getPaymentRequests = asyncWrapper(async (req, res) => {
  const limit = parseInt(req.query.limit, 10) || pagination.limit;
  const page = parseInt(req.query.page, 10) || pagination.page;

  const listAll = await PaymentsRequestsModel.paginate({}, { limit, page });
  return res.status(httpCodes.OK).json({ data: listAll });
});

const payRequest = asyncWrapper(async (req, res) => {
  const existsCommerce = await CommerceModel.findOne({
    email: req.user.email
  });
  if (!existsCommerce)
    return res
      .status(httpCodes.BAD_REQUEST)
      .json({ error: errorMessages.notAccessCreateRequest });

  req.body.emailCommerce = req.user.email;
  req.body.commerce = existsCommerce;
  const resDetail = await PaymentsRequestsModel.create(req.body);

  return res.status(httpCodes.CREATED).json({ data: resDetail });
});

module.exports = {
  getPaymentRequests,
  payRequest
};
