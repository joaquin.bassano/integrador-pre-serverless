const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const userRepository = require('../repositories/users');
const { errorMessages, httpCodes } = require('../constants');

// funciones de autho deben ir en otro servicio (Auth0.js)
const encryptPassword = async password => {
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(password, salt);
  return hash;
};

const generateToken = async user => {
  const token = jwt.sign(
    { id: user._id, email: user.email },
    process.env.JWT_KEY,
    { expiresIn: process.env.JWT_EXPIRATION }
  );
  return token;
};

const findOneUser = async email => userRepository.findOneUser(email);

const createUser = async body => {
  const existsUser = await findOneUser(body.email);
  if (existsUser)
    return {
      isValid: false,
      code: httpCodes.BAD_REQUEST,
      error: errorMessages.userAlreadyExists
    };

  const hashPassword = await encryptPassword(body.password);

  const user = await userRepository.createUser({
    ...body,
    password: hashPassword
  });

  const token = await generateToken(user);

  return { isValid: true, data: { user, token } };
};

module.exports = { createUser, findOneUser, encryptPassword };
