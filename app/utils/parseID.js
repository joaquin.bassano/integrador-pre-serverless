const { mongoose } = require('mongoose');

const parseID = id => {
  const parsedID = mongoose.Types.ObjectId(id);
  return parsedID;
};

module.exports = parseID;
