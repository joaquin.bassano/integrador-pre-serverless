const { UserModel } = require('../models/users');

const findOneUser = async email => UserModel.findOne({ email });

const createUser = async body => UserModel.create(body);

module.exports = { createUser, findOneUser };
