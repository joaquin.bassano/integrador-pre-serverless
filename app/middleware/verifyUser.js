const { errorMessages, httpCodes } = require('../constants');

const verifyUser = (req, res, next) => {
  try {
    const { id } = req.params;
    if (req.user.id !== id)
      return res
        .status(httpCodes.UNAUTHORIZED)
        .json({ error: errorMessages.youCannotAccess });

    next();
  } catch (err) {
    return res.status(httpCodes.FORBIDDEN).json({ error: err });
  }
};

module.exports = verifyUser;
