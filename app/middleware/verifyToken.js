const jwt = require('jsonwebtoken');
const { httpCodes } = require('../constants');

const verifyToken = (req, res, next) => {
  try {
    const bearerHeader = req.headers.authorization || req.headers.Authorization;
    if (!bearerHeader || !bearerHeader.startsWith('Bearer '))
      return res.sendStatus(httpCodes.FORBIDDEN);

    const bearerToken = bearerHeader.split('Bearer ')[1];
    const payload = jwt.verify(bearerToken, process.env.JWT_KEY);
    if (!payload.id || !payload.email)
      return res.sendStatus(httpCodes.FORBIDDEN);

    req.user = payload;
    next();
  } catch (err) {
    return res.status(httpCodes.FORBIDDEN).json({ error: err });
  }
};

module.exports = verifyToken;
