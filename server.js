const app = require('./app');
const logger = require('./app/logger');

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => logger.complete('API lista por el puerto ', PORT));
