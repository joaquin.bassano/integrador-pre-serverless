const mongoose = require('mongoose');
const logger = require('../app/logger');

const dbConnect = () => {
  const { DB_URI, NODE_ENV } = process.env;
  mongoose.connect(
    NODE_ENV === 'test' ? `${DB_URI}/test` : DB_URI,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true
    },
    err => {
      if (!err) logger.success('**** CONEXION CORRECTA ****');
      else logger.error('**** ERROR DE CONEXION ****');
    }
  );
};

module.exports = { dbConnect };
