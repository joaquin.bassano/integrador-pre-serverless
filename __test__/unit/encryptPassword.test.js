const bcrypt = require('bcrypt');
const { encryptPassword } = require('../../app/services/users');

describe('encryptPassword', () => {
  test('with a valid password, should encrypt the password ', async () => {
    const password = await encryptPassword('hola123');
    const isCorrect = await bcrypt.compare('hola123', password);

    expect(password.length).toBeGreaterThan(7);
    expect(isCorrect).toBe(true);
  });
});
