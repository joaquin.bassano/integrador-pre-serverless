require('dotenv').config();

const express = require('express');
const cors = require('cors');

const app = express();
const { dbConnect } = require('./config/mongo');

app.use(cors());
app.use(express.json());

app.use('/api', require('./app/routes'));

dbConnect();

module.exports = app;
